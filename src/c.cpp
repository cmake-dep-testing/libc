
#include <iostream>
#include "c/c.h"

namespace c {
  void addIndentation(uint indentation) {
    for (uint i = 0; i < indentation; ++i) {
      std::cout << "  ";
    }
  }

  void doThingC(uint indentation) {
    addIndentation(indentation);
    std::cout << "C v1" << std::endl;
  };
}
