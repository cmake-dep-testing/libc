cmake_minimum_required(VERSION 3.0.0)
project(libc VERSION 1.0.0)

add_library(c STATIC src/c.cpp)
target_include_directories(c PUBLIC include)
add_library(c::c ALIAS c)
